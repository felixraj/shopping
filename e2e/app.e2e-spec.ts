import { ShopperPage } from './app.po';

describe('shopper App', () => {
  let page: ShopperPage;

  beforeEach(() => {
    page = new ShopperPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
