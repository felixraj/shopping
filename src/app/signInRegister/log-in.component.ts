import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LogInService } from './log-in.service';

@Component({
  selector:'log-in',
  templateUrl:'./log-in.component.html'
})
export class SignIn{
  title="sign in";

  constructor(private logInService:LogInService){}

  signIn(name:string, password:string){
    console.log(name); // todo : remove
    console.log(password); // todo: remove
    this.logInService.signIn(name, password)
        .then((data)=>this.onLogInSuccess(data))
        .catch(this.handleError)
  }

  onLogInSuccess(data):void{
    console.log(data);
  }

  handleError(error):void{
    console.log("Error : "+error);
  }

  dummy(name:string, password:string){
    alert(name+" : "+ password);
  }
}
