import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable'

@Injectable()
export class LogInService{
    /**
     * Login URL :
     * method : 'POST'
     * url : "https://hack-rest.herokuapp.com/users/login"
     * Body: {"userName":"user name here",
     *          "password" : "password here"}
     */

     private log_in_url = "https://hack-rest.herokuapp.com/users/login"

     constructor(private httpClient: HttpClient){}

     signIn(userName:string, password:string):Promise<any>{
         const body = {"userName":userName, "password":password}
        //  return this.httpClient.post(this.log_in_url, body)
        //             .toPromise()
        //             .then((data)=>{console.log(data);data;})
        //             .catch(this.handleError);
        return this.httpClient.post(this.log_in_url, body)
                        .toPromise()
                        .then(data=>{return Promise.resolve(data)}) // so that returned data is available for the called person
                        .catch(this.handleError)
     }

     handleError(error):Promise<any>{
         console.log("Error : "+error);
         return Promise.reject(error.message || error)
     }
}