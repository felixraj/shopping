import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/toPromise';

@Injectable()
export class Register{
  /*
  * Service URLS for Sign up/ Register :
  * method : 'POST',
    url : "https://hack-rest.herokuapp.com/users"
    body :
    {"firstName":"Roshan","lastName":null,"email":"roshan@gmail.com","phone":"8281619989","password":"password""

    Sign In
    Login URL :
    method : 'POST',
    url : "https://hack-rest.herokuapp.com/users/login"
    Body :
    {"userName":"roshan@gmail.com","password":"password"}
  */
  private register_url = "https://hack-rest.herokuapp.com/users/login";

  constructor(private httpClient: HttpClient){}

  register(firstName, lastName, email, phone, password):Promise<any>{
    const body = {"firstName":firstName, "lastName":lastName, "email":email, "phone":phone, "password":password}
    return this.httpClient.post(this.register_url, body)
            .toPromise()
            .then((data)=>{data})
            .catch(this.handleError);
  }

  handleError(error):Promise<any>{
    console.log("Error : "+error);
    return Promise.reject(error.message || error);
  }
}
