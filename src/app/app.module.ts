import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ItemListing } from './itemListing/itemlisting.component';
import { ItemListingServive } from './itemListing/itemlisting.service';
import { AppRoutingModule } from './app-routing.module';
import { Register } from './signInRegister/register.component';
import { SignIn } from './signInRegister/log-in.component';
import { LogInService } from './signInRegister/log-in.service';

@NgModule({
  declarations: [
    AppComponent,
    ItemListing,
    Register,
    SignIn
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule, //<---- a bit old better use the nxt
    HttpClientModule
  ],
  providers: [
    ItemListingServive,
    LogInService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
