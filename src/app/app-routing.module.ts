import  { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';

import { ItemListing } from './itemListing/itemlisting.component';
import { Register } from './signInRegister/register.component';
import { SignIn } from './signInRegister/log-in.component';

const routes: Routes = [
  {path:'list', component:ItemListing},
  {path:'login', component:SignIn},
  {path:'register', component:Register}
];

@NgModule({
  imports:[RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule{}
