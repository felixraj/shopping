export class Item{
  itemId : number;
  itemName : string;
  gtin: any;
  price : number;
  quantity : any;
  shortDesc : string;
  longDesc : string;
  imgURL : string;
}
