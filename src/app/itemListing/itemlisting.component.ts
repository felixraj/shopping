import { Component, OnInit } from '@angular/core';

import { ItemListingServive } from './itemlisting.service';
import {Item} from '../item';

@Component({
  selector:'item-listing',
  templateUrl:'./itemlisting.component.html'
})
export class ItemListing implements OnInit{
  private items: Item[];

  constructor(private listingService: ItemListingServive){}
  ngOnInit():void{
    this.getItems();
  }

  getItems():void{
    this.listingService.getListing().then(data=>{
      console.log("response: "+data);
      this.items=data;
    }).catch((err)=>console.log("Error"+err));
  }
  // console.log(this.listingService.getListing());
}
