import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import  {Item} from '../item';

// const MOCK_DATA:Item[]=[
//   {name:"felix"}
// ]

@Injectable()
export class ItemListingServive{
  private listingUrl = 'https://hack-rest.herokuapp.com/items';
  // private header = new Headers({'Content-Type':'application/json'});

  constructor(private http:Http){}
  // constructor(){}

  // getListingDummy():Promise<Item[]>{
  //   return Promise.resolve(MOCK_DATA);
  // }

  getListing():Promise<Item[]>{
    // return this.getListingDummy();
    return this.http.get(this.listingUrl)
            .toPromise()
            .then(response=>{console.log(response.json() as Item[]); return response.json() as Item[]})
            .catch(this.handleError);
  }

  private handleError(err):Promise<any>{
    console.log('An error Occured ', err);
    return Promise.reject(err.message ||  err);
  }
}
